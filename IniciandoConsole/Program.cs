﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace IniciandoConsole
{
    public class CaixaEletronico
    {
        private readonly List<Conta> _listaDeContas = new List<Conta>();

        public CaixaEletronico()
        {
            _listaDeContas.Add(new Conta("Eliezer", 1));
            _listaDeContas.Add(new Conta("Ovidio", 2));
            _listaDeContas.Add(new Conta("Luiz", 3));
            _listaDeContas.Add(new Conta("Cassio", 4));
            _listaDeContas.Add(new Conta("Daryelson", 5));
            _listaDeContas.Add(new Conta("Vinicius", 6));
            _listaDeContas.Add(new Conta("Yuri Fodao", 7));
            _listaDeContas.Add(new Conta("Chrystian", 8));
        }

        public Conta EncontrarConta(int numeroConta)
        {
            var contaDoCaboco = _listaDeContas.FirstOrDefault(conta => conta.Numero == numeroConta);

            if (contaDoCaboco == null)
            {
                throw new InvalidOperationException("Conta que vc informou não existe");
            }

            return contaDoCaboco;
        }

        public void SacarGrana(decimal valorSacar, Conta conta)
        {
            conta.RetirarDinheiro(valorSacar);
        }

        public void DepositarGrana(decimal valorDepositar, Conta conta)
        {
            conta.DepositarDinheiro(valorDepositar);
        }
    }

    public class Conta
    {
        private decimal _saldo;
        public string Nome { get; }
        public int Numero { get; }

        public Conta(string nome, int numero, decimal saldoInicial = 0)
        {
            Nome = nome;
            Numero = numero;
            _saldo = saldoInicial;
        }

        public void RetirarDinheiro(decimal valorRetirar)
        {
            if (_saldo < valorRetirar)
            {
                throw new InvalidOperationException("Voce nao possui esse saldo");
            }

            _saldo -= valorRetirar;
        }

        public void DepositarDinheiro(decimal valorDepositar)
        {
            _saldo += valorDepositar;
        }
    }

    public class Program
    {
        private static Conta _contaEmAtividade;

        static void Main(string[] args)
        {
            var caixaEletronico = new CaixaEletronico();

            Console.WriteLine("Caixa Eletronico");
            
            while (true)
            {
                try
                {
                    Console.Write("Informe o número da sua conta: ");
                    var numeroConta = int.Parse(Console.ReadLine() ?? "0");

                    _contaEmAtividade = caixaEletronico.EncontrarConta(numeroConta);
                    break;
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine();
                }
            }

            while (true)
            {
                try
                {
                    Console.WriteLine("O que deseja fazer agora " + _contaEmAtividade.Nome + "?");
                    Console.WriteLine("Digite 1 para sacar");
                    Console.WriteLine("Digite 2 para depositar");
                    Console.WriteLine("Digite qualquer outro para sair");

                    var opcaoEscolhida = int.Parse(Console.ReadLine() ?? "0");

                    if (opcaoEscolhida != 1 && opcaoEscolhida != 2)
                    {
                        break;
                    }

                    if (opcaoEscolhida == 1)
                    {
                        Console.Write("Qual valor deseja sacar? ");
                        var valorDigitado = Console.ReadLine();

                        var estiloConversao = new NumberFormatInfo {CurrencyDecimalSeparator = "."};
                        var valorSaque = decimal.Parse(valorDigitado, estiloConversao);

                        caixaEletronico.SacarGrana(valorSaque, _contaEmAtividade);

                        Console.WriteLine("Valor sacado com sucesso");
                    }

                    if (opcaoEscolhida == 2)
                    {
                        Console.Write("Qual valor deseja depositar? ");
                        var valorDigitado = Console.ReadLine();

                        var estiloConversao = new NumberFormatInfo { CurrencyDecimalSeparator = "." };
                        var valorDeposito = decimal.Parse(valorDigitado, estiloConversao);

                        caixaEletronico.DepositarGrana(valorDeposito, _contaEmAtividade);

                        Console.WriteLine("Valor depositado com sucesso");
                    }

                    Console.WriteLine("-");
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            Console.WriteLine("Caixa finalizado... pressione enter para sair.");
            Console.ReadLine();
        }
    }
}