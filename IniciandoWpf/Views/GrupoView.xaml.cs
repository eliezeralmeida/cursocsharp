﻿using IniciandoWpf.Core.Repositorios;
using IniciandoWpf.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IniciandoWpf.Views
{
    /// <summary>
    /// Lógica interna para GrupoView.xaml
    /// </summary>
    public partial class GrupoView : Window
    {
        public GrupoView()
        {
            InitializeComponent();
        }

        private void ClickSalvarGrupo(object sender, RoutedEventArgs e)
        {

            try
            {
                ValidarInformacoesTela();

                var grupo = new Grupo(InputNome.Text);

                var repositorio = new Gruporepositorio();
                repositorio.SalvarGrupo(grupo);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputNome.Text))
            {
                InputNome.Focus();
                throw new InvalidOperationException("Login é obrigatório");
            }

            

        }
    }
}

