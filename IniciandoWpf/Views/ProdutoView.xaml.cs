﻿using IniciandoWpf.Core.Repositorios;
using IniciandoWpf.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IniciandoWpf.Views
{
    /// <summary>
    /// Lógica interna para ProdutoView.xaml
    /// </summary>
    public partial class ProdutoView : Window
    {
        public ProdutoView()
        {
            InitializeComponent();
        }

        private void ClickProdutoHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarInformacoesTela();

                var style = new NumberFormatInfo() {
                    NumberDecimalSeparator = ","
                };
                var preco = decimal.Parse(InputPreco.Text, style);
                var produto = new Produto(InputDescricao.Text, preco);

                var repositorio = new ProdutoRepositorio();
                repositorio.SalvarProduto(produto);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputDescricao.Text))
            {
                InputDescricao.Focus();
                throw new InvalidOperationException("Descrição é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputPreco.Text))
            {
                InputPreco.Focus();
                throw new InvalidOperationException("Preço é obrigatório");
            }

        }
    }
}
