﻿using IniciandoWpf.Core.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IniciandoWpf.Views
{
    /// <summary>
    /// Lógica interna para GrupoListagemView.xaml
    /// </summary>
    public partial class GrupoListagemView : Window
    {
        public GrupoListagemView()
        {
            InitializeComponent();
        }

        private void LoadedHandler(object sender, RoutedEventArgs e)
        {
            var repositorio = new Gruporepositorio();
            GridGrupo.ItemsSource = repositorio.TodosGrupos();
        }
    }
}
