﻿using IniciandoWpf.Core.Repositorios;
using System.Windows;

namespace IniciandoWpf.Views
{
    /// <summary>
    /// Lógica interna para ClienteListagemViews.xaml
    /// </summary>
    public partial class ClienteListagemViews : Window
    {
        public ClienteListagemViews()
        {
            InitializeComponent();
            
        }


        private void LoadedHandler(object sender, RoutedEventArgs e)
        {
            var repositorio = new ClienteRepositorio();
            GridCliente.ItemsSource = repositorio.TodosClientes();

        }
    }
}


