﻿using System.Collections.Generic;
using System.Windows;
using IniciandoWpf.Core.Db;
using IniciandoWpf.Core.Repositorios;
using IniciandoWpf.Models;

namespace IniciandoWpf.Views
{
    public partial class UsuarioListagemView : Window
    {
        public UsuarioListagemView()
        {
            InitializeComponent();
        }

        private void LoadedHandler(object sender, RoutedEventArgs e)
        {
            var repositorio = new UsuarioRepositorio();
            GridUsuario.ItemsSource = repositorio.TodosUsuarios();
        }
    }
}