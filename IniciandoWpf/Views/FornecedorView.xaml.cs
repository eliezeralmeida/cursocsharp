﻿using IniciandoWpf.Core.Repositorios;
using IniciandoWpf.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IniciandoWpf.Views
{
    /// <summary>
    /// Lógica interna para Window1.xaml
    /// </summary>
    public partial class FornecedorView : Window
    {
        public FornecedorView()
        {
            InitializeComponent();
        }

        private void ClickSalvarHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarInformacoesTela();
                 
                var fornecedor = new Fornecedor(InputNome.Text, InputNomeFantazia.Text, InputCnpj.Text, InputNumTelefone.Text);

                var repositorio = new FornecedorRepositorio();
                repositorio.SalvarFornecedor(fornecedor);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputNome.Text))
            {
                InputNome.Focus();
                throw new InvalidOperationException("Campo nome não pode ficar vazil");
            }

            if (string.IsNullOrWhiteSpace(InputNomeFantazia.Text))
            {
                InputNomeFantazia.Focus();
                throw new InvalidOperationException("Desculpe!, Mas o campo nome fantazia não pode ficar vazil");
            }

            if (string.IsNullOrWhiteSpace(InputCnpj.Text))
            {
                InputNomeFantazia.Focus();
                throw new InvalidOperationException("Desculpe!, Mas o campo cnpj não pode ficar vazil");
            }

            if (string.IsNullOrWhiteSpace(InputNumTelefone.Text))
            {
                InputNomeFantazia.Focus();
                throw new InvalidOperationException("Desculpe!, Mas o campo telefone não pode ficar vazil");
            }

        }
    }
}
