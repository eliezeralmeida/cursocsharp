﻿using System.Windows;

namespace IniciandoWpf.Views
{
    public partial class LoginView : Window
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void ClickBotaoEntrarHandler(object sender, RoutedEventArgs e)
        {
            if (InputUsuario.Text == "admin" && InputSenha.Password == "admin")
            {
                new MenuView().Show();
                Close();
                return;
            }

            MessageBox.Show("Credenciais inválidas", "Login", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}