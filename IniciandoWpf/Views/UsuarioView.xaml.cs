﻿using System;
using System.Windows;
using IniciandoWpf.Core.Repositorios;
using IniciandoWpf.Models;

namespace IniciandoWpf.Views
{
    public partial class UsuarioView
    {
        public UsuarioView()
        {
            InitializeComponent();
        }

        private void ClickSalvarHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarInformacoesTela();

                var usuario = new Usuario(InputLogin.Text, InputSenha.Password);

                var repositorio = new UsuarioRepositorio();
                repositorio.SalvarUsuario(usuario);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputLogin.Text))
            {
                InputLogin.Focus();
                throw new InvalidOperationException("Login é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputSenha.Password))
            {
                InputSenha.Focus();
                throw new InvalidOperationException("Senha é obrigatório");
            }

        }
    }
}