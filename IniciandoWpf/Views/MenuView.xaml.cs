﻿using System.Windows;
using IniciandoWpf.Core.Db;

namespace IniciandoWpf.Views
{
    public partial class MenuView
    {
        public MenuView()
        {
            InitializeComponent();
        }

        private void ClickUsuarioHandler(object sender, RoutedEventArgs e)
        {
            new UsuarioView().ShowDialog();
        }

        private void ClickListUsuarioHandler(object sender, RoutedEventArgs e)
        {
            new UsuarioListagemView().ShowDialog();
        }

        private void ClickFornecedorHandler(object sender, RoutedEventArgs e)
        {
            new FornecedorView().ShowDialog();
        }

        private void ClickListFornecedorHandler(object sender, RoutedEventArgs e)
        {
            new FornecedorListagemView().ShowDialog();
        }

        private void ClickCadastroProduto(object sender, RoutedEventArgs e)
        {
            new ProdutoView().ShowDialog();
        }

        private void ClickListagemProduto(object sender, RoutedEventArgs e)
        {
            new ProdutoListagemView().ShowDialog();
        }

        private void ClickCliente(object sender, RoutedEventArgs e)
        {
            new ClientesViews().ShowDialog();
        }

        private void BtnListaCliente(object sender, RoutedEventArgs e)
        {
            new ClienteListagemViews().ShowDialog();
        }

        private void ClickCadastroGrupo(object sender, RoutedEventArgs e)
        {
            new GrupoView().ShowDialog();
        }

        private void ClickListagemGrupo(object sender, RoutedEventArgs e)
        {
            new GrupoListagemView().ShowDialog();
        }
    }
}