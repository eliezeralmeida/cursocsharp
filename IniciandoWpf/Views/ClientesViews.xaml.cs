﻿using System;
using System.Windows;
using IniciandoWpf.Core.Repositorios;
using IniciandoWpf.Models;

namespace IniciandoWpf.Views
{
    public partial class ClientesViews
    {
        public ClientesViews()
        {
            InitializeComponent();
        }


        private void ClickSalvarHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarInformacoesTela();

                var cliente = new Cliente(InputNome.Text, InputEmail.Text);

                var repositorio = new ClienteRepositorio();
                repositorio.SalvarCliente(cliente);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputNome.Text))
            {
                InputNome.Focus();
                throw new InvalidOperationException("Nome é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputEmail.Text))
            {
                InputEmail.Focus();
                throw new InvalidOperationException("Email é obrigatório");
            }

        }
    }
}