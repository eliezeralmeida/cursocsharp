﻿using System.Windows;
using IniciandoWpf.Core.Db;

namespace IniciandoWpf
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            CriarBancoDados();
            base.OnStartup(e);
        }

        private void CriarBancoDados()
        {
            // Instanciando o objeto do banco
            var bd = new BancoDados(ConexaoFactory.Instancia);
            // Metodo que verifica a existencia do banco, se não existir cria o mesmo.
            bd.CriarBd();
            // Metodo verifica se a tabela Usuario existe, se não cria a mesma.
            bd.CriarTabela(new TabelaUsuario());
            // Metodo verifica se a tabela Fornecedor existe, se não cria a mesma.
            bd.CriarTabela(new TabelaFornecedor());
            // Metodo verifica se a tabela Grupo existe, se não cria a mesma.
            bd.CriarTabela(new TabelaGrupo());
            // Metodo verifica se a tabela Produto existe, se não cria a mesma.
            bd.CriarTabela(new TabelaProduto());
            // Metodo verifica se a tabela cliente existe, se não cria a mesma.
            bd.CriarTabela(new TabelaCliente());

        }
    }
}