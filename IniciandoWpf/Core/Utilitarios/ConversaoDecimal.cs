﻿using System.Globalization;

namespace IniciandoWpf.Core.Utilitarios
{
    public static class ConversaoDecimal
    {
        public static decimal ConverteTexto(string texto)
        {
            var style = new NumberFormatInfo
            {
                NumberDecimalSeparator = ","
            };

            var convertido =  decimal.Parse(texto, style);

            return convertido;
        }
    }
}