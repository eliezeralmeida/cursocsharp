﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using IniciandoWpf.Core.Db;
using IniciandoWpf.Models;
using IniciandoWpf.Views;

// instanciando oname space 
namespace IniciandoWpf.Core.Repositorios
{
    class FornecedorRepositorio
    {
        // variavel de connexão
        private readonly ConexaoFactory _conexaoFactory;
        private IList<Fornecedor> fornecedor;

        public FornecedorRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public IList<Fornecedor> TodosFornecedor()
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "select * from fornecedor";

                var reader = cmd.ExecuteReader();
                var fornecedores = new List<Fornecedor>();

                while (reader.Read())
                {
                    var oNome           = reader.GetOrdinal("nome");
                    var oNomeFantazia   = reader.GetOrdinal("nomefantazia");
                    var oCnpj           = reader.GetOrdinal("cnpj");
                    var oNumTelefone    = reader.GetOrdinal("numTelefone");

                    var oId         = reader.GetOrdinal("id");
                    
                    // Cria a construção 
                    var fornecedor = new Fornecedor(reader.GetString(oNome), reader.GetString(oNomeFantazia), reader.GetString(oCnpj), reader.GetString(oNumTelefone))
                    {
                        Id = reader.GetInt32(oId)
                    };

                    // Adiciona 
                    fornecedores.Add(fornecedor);

                }

                return fornecedores;
            }
        }


        public void SalvarFornecedor(Fornecedor fornecedor)
        {
            // Cria usando a variavel using para abrir e fechar o banco de dados.
            using (var conexao = _conexaoFactory.Abrir())
            {
                var insert = "insert into fornecedor(nome,nomeFantazia,cnpj,numTelefone) values(:nome, :nomeFantazia, :cnpj, :numTelefone)";

                var cmdInsert = new SQLiteCommand(insert, conexao);
                cmdInsert.Parameters.Add(":nome", DbType.String).Value          = fornecedor.Nome;
                cmdInsert.Parameters.Add(":nomeFantazia", DbType.String).Value  = fornecedor.NomeFantazia;
                cmdInsert.Parameters.Add(":cnpj", DbType.String).Value          = fornecedor.Cnpj;
                cmdInsert.Parameters.Add(":numTelefone", DbType.String).Value   = fornecedor.NumTelefone;

                cmdInsert.ExecuteNonQuery();

                var select = "select max(id) from fornecedor";
                var cmdSelect = new SQLiteCommand(select, conexao);

                fornecedor.Id = int.Parse(cmdSelect.ExecuteScalar().ToString());
            }
        }
    }
}
