﻿using IniciandoWpf.Core.Db;
using IniciandoWpf.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniciandoWpf.Core.Repositorios
{
    public class Gruporepositorio
    {
        private readonly ConexaoFactory _conexaoFactory;

        public Gruporepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public IList<Grupo> TodosGrupos()
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "select * from grupo";

                var reader = cmd.ExecuteReader();
                var grupos = new List<Grupo>();

                while (reader.Read())
                {
                    var oNome = reader.GetOrdinal("nome");
                    var oId = reader.GetOrdinal("id");

                    var grupo = new Grupo(reader.GetString(oNome))
                    {
                        Id = reader.GetInt32(oId)
                    };

                    grupos.Add(grupo);
                }

                return grupos;
            }
        }

        public void SalvarGrupo(Grupo grupo)
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var insert = "insert into grupo(nome) values(:nome)";

                var cmdInsert = new SQLiteCommand(insert, conexao);
                cmdInsert.Parameters.Add(":nome", DbType.String).Value = grupo.Nome;
                

                cmdInsert.ExecuteNonQuery();

                var select = "select max(id) from grupo";
                var cmdSelect = new SQLiteCommand(select, conexao);

                grupo.Id = int.Parse(cmdSelect.ExecuteScalar().ToString());
            }
        }
    }
}

