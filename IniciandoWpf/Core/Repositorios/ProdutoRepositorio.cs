﻿using IniciandoWpf.Core.Db;
using IniciandoWpf.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniciandoWpf.Core.Repositorios
{
    public class ProdutoRepositorio
    {
        private readonly ConexaoFactory _conexaoFactory;

        public ProdutoRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public IList<Produto> TodosProdutos()
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "select * from produto";

                var reader = cmd.ExecuteReader();
                var produtos = new List<Produto>();

                while (reader.Read())
                {
                    var oDescricao = reader.GetOrdinal("descricao");
                    var oPreco = reader.GetOrdinal("preco");
                    var oId = reader.GetOrdinal("id");

                                    
                    var produto = new Produto(reader.GetString(oDescricao), reader.GetDecimal(oPreco))
                    {
                        Id = reader.GetInt32(oId)
                    };

                    produtos.Add(produto);
                }

                return produtos;
            }
        }

        public void SalvarProduto(Produto produto )
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var insert = "insert into produto(descricao, preco) values(:descricao, :preco)";

                var cmdInsert = new SQLiteCommand(insert, conexao);
                cmdInsert.Parameters.Add(":descricao", DbType.String).Value = produto.Descricao;
                cmdInsert.Parameters.Add(":preco", DbType.Decimal).Value = produto.Preco;

                cmdInsert.ExecuteNonQuery();

                var select = "select max(id) from produto";
                var cmdSelect = new SQLiteCommand(select, conexao);

                produto.Id = int.Parse(cmdSelect.ExecuteScalar().ToString());
            }
        }
    }
}
