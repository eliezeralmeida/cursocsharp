﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using IniciandoWpf.Core.Db;
using IniciandoWpf.Models;

namespace IniciandoWpf.Core.Repositorios
{
    public class UsuarioRepositorio
    {
        private readonly ConexaoFactory _conexaoFactory;

        public UsuarioRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public IList<Usuario> TodosUsuarios()
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "select * from usuario";

                var reader = cmd.ExecuteReader();
                var usuarios = new List<Usuario>();

                while (reader.Read())
                {
                    var oLogin = reader.GetOrdinal("login");
                    var oSenha = reader.GetOrdinal("senha");
                    var oId = reader.GetOrdinal("id");

                    var usuario = new Usuario(reader.GetString(oLogin), reader.GetString(oSenha))
                    {
                        Id = reader.GetInt32(oId)
                    };

                    usuarios.Add(usuario);
                }

                return usuarios;
            }
        }

        public void SalvarUsuario(Usuario usuario)
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var insert = "insert into usuario(login,senha) values(:login, :senha)";

                var cmdInsert = new SQLiteCommand(insert, conexao);
                cmdInsert.Parameters.Add(":login", DbType.String).Value = usuario.Login;
                cmdInsert.Parameters.Add(":senha", DbType.String).Value = usuario.Senha;

                cmdInsert.ExecuteNonQuery();

                var select = "select max(id) from usuario";
                var cmdSelect = new SQLiteCommand(select, conexao);

                usuario.Id = int.Parse(cmdSelect.ExecuteScalar().ToString());
            }
        }
    }
}