﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using IniciandoWpf.Core.Db;
using IniciandoWpf.Models;

namespace IniciandoWpf.Core.Repositorios
{
    public class ClienteRepositorio
    {
        private readonly ConexaoFactory _conexaoFactory;

        public ClienteRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public IList<Cliente> TodosClientes()
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "select * from cliente";

                var reader = cmd.ExecuteReader();
                var clientes = new List<Cliente>();

                while (reader.Read())
                {
                    var oNome = reader.GetOrdinal("nome");
                    var oEmail = reader.GetOrdinal("email");
                    var oId = reader.GetOrdinal("id");

                    var cliente = new Cliente(reader.GetString(oNome), reader.GetString(oEmail))
                    {
                        Id = reader.GetInt32(oId)
                    };

                    clientes.Add(cliente);
                }

                return clientes;
            }
        }

        public void SalvarCliente(Cliente cliente)
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var insert = "insert into cliente(nome,email) values(:nome, :email)";

                var cmdInsert = new SQLiteCommand(insert, conexao);
                cmdInsert.Parameters.Add(":nome", DbType.String).Value = cliente.Nome;
                cmdInsert.Parameters.Add(":email", DbType.String).Value = cliente.Email;

                cmdInsert.ExecuteNonQuery();

                var select = "select max(id) from cliente";
                var cmdSelect = new SQLiteCommand(select, conexao);

                cliente.Id = int.Parse(cmdSelect.ExecuteScalar().ToString());
            }
        }
    }
}
