﻿using System.Data.SQLite;

namespace IniciandoWpf.Core.Db
{
    public class ConexaoFactory
    {
        private static ConexaoFactory _instancia;
        public static ConexaoFactory Instancia => _instancia ?? (_instancia = new ConexaoFactory());

        private ConexaoFactory()
        {
        }

        public SQLiteConnection Abrir()
        {
            var conexao = new SQLiteConnection(@"Data Source=C:\Temp\cursocsharp.db;");
            return conexao.OpenAndReturn();
        }
    }
}