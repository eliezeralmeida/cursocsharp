﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniciandoWpf.Core.Db
{
    class TabelaCliente : ITabela
    {
        public string NomeTabela =>"cliente";

        public IList<string> Colunas { get; } = new List<string>();

        public TabelaCliente()
        {
            Colunas.Add("id integer not null primary key autoincrement");
            Colunas.Add("nome varchar(255) not null");
            Colunas.Add("email varchar(255) not null");
        }
    }
}





