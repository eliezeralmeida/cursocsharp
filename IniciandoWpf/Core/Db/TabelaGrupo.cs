﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniciandoWpf.Core.Db
{
  

        public class TabelaGrupo : ITabela
        {
            public string NomeTabela { get; } = "grupo";
            public IList<string> Colunas { get; } = new List<string>();

            public TabelaGrupo()
            {
                Colunas.Add("id integer not null primary key autoincrement");
                Colunas.Add("nome varchar(255) not null");
                
            }
        }
    }

