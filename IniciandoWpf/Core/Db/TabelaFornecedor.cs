﻿using System.Collections.Generic;

namespace IniciandoWpf.Core.Db
{
    public class TabelaFornecedor : ITabela
    {
        public string NomeTabela { get; } = "fornecedor";
        public IList<string> Colunas { get; } = new List<string>();

        public TabelaFornecedor()
        {
            Colunas.Add("id integer not null primary key autoincrement");
            Colunas.Add("nome varchar(255) not null");
            Colunas.Add("nomeFantazia varchar(255) not null");
            Colunas.Add("cnpj varchar(255) not null");
            Colunas.Add("numTelefone varchar(255) not null");
        }
    }
}