﻿using System.Collections.Generic;

namespace IniciandoWpf.Core.Db
{
    public class TabelaUnidadeMedida : ITabela
    {
        public string NomeTabela { get; } = "unidade_medida";
        public IList<string> Colunas { get; } = new List<string>();
    }
}