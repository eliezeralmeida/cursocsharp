﻿using System;
using System.Data.SQLite;
using System.IO;

namespace IniciandoWpf.Core.Db
{
    public class BancoDados
    {
        private readonly ConexaoFactory _conexaoFactory;

        public BancoDados(ConexaoFactory conexaoFactory)
        {
            _conexaoFactory = conexaoFactory;
        }

        public void CriarBd()
        {
            if (!File.Exists(@"C:\Temp\cursocsharp.db"))
            {
                if (!Directory.Exists(@"C:\Temp"))
                {
                    Directory.CreateDirectory(@"C:\Temp");
                }

                SQLiteConnection.CreateFile(@"C:\temp\cursocsharp.db");
            }
        }

        public void CriarTabela(ITabela tabela)
        {
            var createTable = "create table if not exists "
                + tabela.NomeTabela + "(" + string.Join(",", tabela.Colunas) + ")";

            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = createTable;
                cmd.ExecuteNonQuery();
            }
        }
    }
}