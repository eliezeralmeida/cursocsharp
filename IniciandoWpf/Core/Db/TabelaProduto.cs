﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniciandoWpf.Core.Db
{
    class TabelaProduto : ITabela
    {
        public string NomeTabela => "Produto";

        public IList<string> Colunas { get; } = new List<String>();

        public TabelaProduto()
        {
            Colunas.Add("id integer not null primary key autoincrement");
            Colunas.Add("descricao varchar(255) not null");
            Colunas.Add("preco decimal(10,2) not null");
        }
    }
}
