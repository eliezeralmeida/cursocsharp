﻿using System.Collections.Generic;

namespace IniciandoWpf.Core.Db
{
    public interface ITabela
    {
        string NomeTabela { get; }
        IList<string> Colunas { get; }
    }
}