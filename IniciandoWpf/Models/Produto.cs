﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniciandoWpf.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public decimal Preco { get; set; }

        public Produto(string descricao, decimal preco)
        {
            Descricao = descricao;
            Preco = preco;
        }

    }
}
