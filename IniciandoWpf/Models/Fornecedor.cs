﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniciandoWpf.Models
{
    class Fornecedor
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string NomeFantazia { get; set; }
        public string Cnpj { get; set; }
        public string NumTelefone { get; set; }

        public Fornecedor(string nome, string nomeFantazia, string cnpj, string numTelefone)
        {
            Nome = nome;
            NomeFantazia = nomeFantazia;
            Cnpj = cnpj;
            NumTelefone = numTelefone;
        }
    }
}


