﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Locadora.Core.Db;
using Locadora.Core.Db.Tabelas;

namespace Locadora
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var bancoDados = new BancoDados();
            bancoDados.CriarBd();
            bancoDados.CriarTabela(new TabelaUsuario());
            bancoDados.CriarTabela(new TabelaCliente());

            bancoDados.CriarTabela(new TabelaFilmes());

            base.OnStartup(e);
        
        }
    }
}
