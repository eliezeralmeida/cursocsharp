﻿using System;
using System.Windows;
using Locadora.Core.Conversores;
using Locadora.Core.Models;
using Locadora.Core.Repositorios;

namespace Locadora.Views
{
    public partial class FilmeView
    {
        public FilmeView()
        {
            InitializeComponent();
        }

        private void BotaoSalvar(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarInformacoesTela();

                var precoDia = ConversorDecimal.ConverteDeString(InputPrecoDia.Text);

                var filme = new Filme(
                    InputNomeFilme.Text,
                    InputDuracao.Text,
                    InputGenero.Text,
                    precoDia,
                    InputResumo.Text);

                var repositorio = new FilmeRepositorio();
                repositorio.SalvarFilme(filme);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputNomeFilme.Text))
            {
                InputNomeFilme.Focus();
                throw new InvalidOperationException("Nome Do Filme é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputDuracao.Text))
            {
                InputDuracao.Focus();
                throw new InvalidOperationException("Duração é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputGenero.Text))
            {
                InputGenero.Focus();
                throw new InvalidOperationException("Genero é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputPrecoDia.Text))
            {
                InputPrecoDia.Focus();
                throw new InvalidOperationException("Preço dia é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputResumo.Text))
            {
                InputResumo.Focus();
                throw new InvalidOperationException("Resumo é obrigatório");
            }
        }


        private void BotaoExcluir(object sender, RoutedEventArgs e)
        {
        }
    }
}