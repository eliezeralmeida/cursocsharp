﻿using System;
using System.Windows;
using Locadora.Core.Models;
using Locadora.Core.Repositorios;

namespace Locadora.Views
{
    public partial class UsuarioView
    {
        public UsuarioView()
        {
            InitializeComponent();
        }

        private void ButtonSalvar(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarInformacoesTela();

                var usuario = new Usuario
                {
                    Login = InputLogin.Text,
                    Senha = InputSenha.Text
                };

                var repositorio = new UsuarioRepositorio();
                repositorio.SalvarUsuario(usuario);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputLogin.Text))
            {
                InputLogin.Focus();
                throw new InvalidOperationException("Login é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputSenha.Text))
            {
                InputSenha.Focus();
                throw new InvalidOperationException("Senha é obrigatório");
            }
        }
    }
}