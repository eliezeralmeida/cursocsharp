﻿using System;
using System.Windows;
using Locadora.Core.Repositorios;

namespace Locadora.Views
{
    public partial class LoginView : Window
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void ClickLogar(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarDadosTela();
                
                var repositorio = new LoginRepositorio();
                var usuario = repositorio.ObterUsuarioPeloLogin(InputLogin.Text);

                if (usuario == null || usuario.Senha != InputSenha.Password)
                {
                    MessageBox.Show("Usuário ou Senha incorreta", "Erro de login");
                    return;
                }

                new MenuLocadora().Show();
                Close();
            }
            catch (InvalidOperationException exception)
            {
                MessageBox.Show(exception.Message, "Aviso");
            }
        }

        private void ValidarDadosTela()
        {
            if (string.IsNullOrWhiteSpace(InputLogin.Text))
            {
                throw new InvalidOperationException("Login é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputSenha.Password))
            {
                throw new InvalidOperationException("Senha é obrigatório");
            }
        }
    }
}
