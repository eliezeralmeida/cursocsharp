﻿using System.Windows;
using Locadora.Core.Repositorios;

namespace Locadora.Views
{
    public partial class UsuarioListagemView
    {
        public UsuarioListagemView()
        {
            InitializeComponent();
        }

        private void LoadedHandler(object sender, RoutedEventArgs e)
        {
            AtualizarDadosTela();
        }

        private void AtualizarDadosTela()
        {
            var repositorio = new UsuarioRepositorio();
            var todosUsuarios = repositorio.TodosUsuarios();

            GridUsuario.ItemsSource = todosUsuarios;
        }

        private void ClickNovoUsuario(object sender, RoutedEventArgs e)
        {
            new UsuarioView().ShowDialog();

            AtualizarDadosTela();
        }
    }
}