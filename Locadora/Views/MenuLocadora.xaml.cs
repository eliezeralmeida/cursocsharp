﻿using System.Windows;

namespace Locadora.Views
{
    public partial class MenuLocadora
    {
        public MenuLocadora()
        {
            InitializeComponent();
        }

        private void ClickTeste(object sender, RoutedEventArgs e)
        {
            new LoginView().ShowDialog();
        }

        private void ClickCliente(object sender, System.Windows.RoutedEventArgs e)
        {
            new ClienteView().ShowDialog();
        }

        private void CadastroFilme(object sender, System.Windows.RoutedEventArgs e)
        {
            new FilmeView().ShowDialog(); 
        }

        private void ButtonLocar(object sender, System.Windows.RoutedEventArgs e)
        {
            new locarViews().ShowDialog();
        }

        private void ButtonUsuario(object sender, System.Windows.RoutedEventArgs e)
        {
            new UsuarioView().ShowDialog();
        }

        private void ClickListLocacaoHandler(object sender, RoutedEventArgs e)
        {
            new LocacaoView().ShowDialog();
        }

        private void ClickListUsuarioHandler(object sender, RoutedEventArgs e)
        {
            new UsuarioListagemView().ShowDialog();
        }
    }
}