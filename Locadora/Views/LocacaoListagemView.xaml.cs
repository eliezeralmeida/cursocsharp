﻿using System.Windows;
using Locadora.Core.Repositorios;

namespace Locadora.Views
{
    public partial class LocacaoView
    {
        public LocacaoView()
        {
            InitializeComponent();
        }

        private void LoadedHandler(object sender, RoutedEventArgs e)
        {
            var repositorio = new LocacaoRepositorio();
            GridLocacao.ItemsSource = repositorio.TodasLocacoes();
        }
    }
}