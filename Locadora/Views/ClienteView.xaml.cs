﻿using System;
using System.Windows;
using Locadora.Core.Models;
using Locadora.Core.Repositorios;

namespace Locadora.Views
{
    public partial class ClienteView
    {
        public ClienteView()
        {
            InitializeComponent();
        }

        private void ClickBotaoSalvarHandler(object sender, RoutedEventArgs e)
        {
            try
            {
                ValidarInformacoesTela();

                var cliente = new Cliente(InputNome.Text, InputCpf.Text, InputRg.Text);

                var repositorio = new ClienteRepositorio();
                repositorio.SalvarCliente(cliente);

                Close();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ValidarInformacoesTela()
        {
            if (string.IsNullOrWhiteSpace(InputNome.Text))
            {
                InputNome.Focus();
                throw new InvalidOperationException("Nome é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputCpf.Text))
            {
                InputCpf.Focus();
                throw new InvalidOperationException("CPF é obrigatório");
            }

            if (string.IsNullOrWhiteSpace(InputRg.Text))
            {
                InputRg.Focus();
                throw new InvalidOperationException("RG é obrigatório");
            }
        }

        private void ClickBotaoExcluirHandler(object sender, RoutedEventArgs e)
        {
        }
    }
}