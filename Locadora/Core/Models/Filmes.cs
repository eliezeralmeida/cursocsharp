﻿namespace Locadora.Core.Models
{
    public class Filme
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Genero { get; set; }
        public string Duracao { get; set; }
        public decimal PrecoDia { get; set; }
        public string Resumo { get; set; }

        public Filme(string nome, string genero, string duracao, decimal precodia, string resumo)
        {
            Nome = nome;
            Genero = genero;
            Duracao = duracao;
            PrecoDia = precodia;
            Resumo = resumo;
        }
    }
}