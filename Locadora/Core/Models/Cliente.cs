﻿namespace Locadora.Core.Models
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Rg { get; set; }

        public Cliente(string nome, string cpf, string rg)
        {
            Nome = nome;
            Cpf = cpf;
            Rg = rg;
        }
    }
}
