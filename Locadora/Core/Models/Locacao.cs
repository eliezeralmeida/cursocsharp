﻿using System;

namespace Locadora.Core.Models
{
    public class Locacao
    {
        public Filme Filme { get; set; }
        public Cliente Cliente { get; set; }
        public decimal Valor { get; set; }
    }
}
