﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Locadora.Core.Db.Tabelas
{
    class TabelaCliente: ITabela
    {
        public string NomeTabela => "cliente";

        public IList<string> Colunas { get; } = new List<string>();

        public TabelaCliente()
        {
            Colunas.Add("id integer not null primary key autoincrement");
            Colunas.Add("nome varchar(255) not null");
            Colunas.Add("cpf varchar(11) not null");
            Colunas.Add("rg varchar(20) not null");
        }
    }
}
