﻿using System.Collections.Generic;

namespace Locadora.Core.Db.Tabelas
{
    public class TabelaUsuario : ITabela
    {
        public string NomeTabela { get; } = "usuario";
        public IList<string> Colunas { get; } = new List<string>();

        public TabelaUsuario()
        {
            Colunas.Add("id integer not null primary key autoincrement");
            Colunas.Add("login varchar(255) not null");
            Colunas.Add("senha varchar(255) not null");
        }
    }
}