﻿using System.Collections.Generic;

namespace Locadora.Core.Db.Tabelas
{
    public class TabelaFilmes : ITabela
    {
        public string NomeTabela => "filmes";
        public IList<string> Colunas { get; } = new List<string>();

        public TabelaFilmes()
        {
            Colunas.Add("id integer not null primary key autoincrement");
            Colunas.Add("nome_filme varchar(255) not null");
            Colunas.Add("duracao varchar(255) not null");
            Colunas.Add("genero varchar(255) not null");
            Colunas.Add("preco_dia decimal(10,2) not null");
            Colunas.Add("resumo varchar(255) not null");
        }
    }
}