﻿using System;
using System.Data.SQLite;
using System.IO;

namespace Locadora.Core.Db
{
    public class BancoDados
    {
        public const string CAMINHO_BASE = @"C:\Temp";
        public const string NOME_BANCO = "locadora.db";

        private readonly ConexaoFactory _conexaoFactory;

        public BancoDados()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public void CriarBd()
        {
            var arquivoBanco = Path.Combine(CAMINHO_BASE, NOME_BANCO);

            if (!File.Exists(arquivoBanco))
            {
                if (!Directory.Exists(CAMINHO_BASE))
                {
                    Directory.CreateDirectory(CAMINHO_BASE);
                }

                SQLiteConnection.CreateFile(arquivoBanco);
            }
        }

        public void CriarTabela(ITabela tabela)
        {
            var createTable = "create table if not exists "
                + tabela.NomeTabela + "(" + string.Join(",", tabela.Colunas) + ")";

            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = createTable;
                cmd.ExecuteNonQuery();
            }
        }
    }
}