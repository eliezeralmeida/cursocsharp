﻿using System.Collections.Generic;

namespace Locadora.Core.Db
{
    public interface ITabela
    {
        string NomeTabela { get; }
        IList<string> Colunas { get; }
    }
}