﻿using System.Data.SQLite;
using System.IO;

namespace Locadora.Core.Db
{
    public class ConexaoFactory
    {
        private static ConexaoFactory _instancia;
        public static ConexaoFactory Instancia => _instancia ?? (_instancia = new ConexaoFactory());

        private ConexaoFactory()
        {
        }

        public SQLiteConnection Abrir()
        {
            var db = Path.Combine(BancoDados.CAMINHO_BASE, BancoDados.NOME_BANCO);
            var conexao = new SQLiteConnection("Data Source=" + db + ";");
            return conexao.OpenAndReturn();
        }
    }
}