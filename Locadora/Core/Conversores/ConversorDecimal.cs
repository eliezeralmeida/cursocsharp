﻿using System.Globalization;

namespace Locadora.Core.Conversores
{
    public static class ConversorDecimal
    {
        public static decimal ConverteDeString(string input)
        {
            var info = new NumberFormatInfo
            {
                NumberGroupSeparator = ".",
                NumberDecimalSeparator = ","
            };

            var valor = decimal.Parse(input, info);

            return valor;
        }
    }
}