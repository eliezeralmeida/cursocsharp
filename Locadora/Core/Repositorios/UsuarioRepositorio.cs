﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using Locadora.Core.Db;
using Locadora.Core.Models;

namespace Locadora.Core.Repositorios
{
    public class UsuarioRepositorio
    {
        private readonly ConexaoFactory _conexaoFactory;

        public UsuarioRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public IList<Usuario> TodosUsuarios()
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "SELECT id, login FROM usuario";

                var reader = cmd.ExecuteReader();
                var usuarios = new List<Usuario>();

                while (reader.Read())
                {
                    var usuario = new Usuario
                    {
                        Id = reader.GetInt32(0),
                        Login = reader.GetString(1)
                    };


                    usuarios.Add(usuario);
                }

                return usuarios;
            }
        }

        public void SalvarUsuario(Usuario usuario)
        {
            using (var conexao = ConexaoFactory.Instancia.Abrir())
            {
                var insert = "insert into usuario(login,senha) values(:login, :senha)";

                var cmdInsert = new SQLiteCommand(insert, conexao);
                cmdInsert.Parameters.Add(":login", DbType.String).Value = usuario.Login;
                cmdInsert.Parameters.Add(":senha", DbType.String).Value = usuario.Senha;

                cmdInsert.ExecuteNonQuery();

                var select = "select max(id) from usuario";
                var cmdSelect = new SQLiteCommand(select, conexao);

                usuario.Id = int.Parse(cmdSelect.ExecuteScalar().ToString());
            }
        }
    }
}