﻿
using Locadora.Core.Db;
using Locadora.Core.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System;

namespace Locadora.Core.Repositorios
{
    public class FilmeRepositorio
    {
        private ConexaoFactory _conexaoFactory;

        public FilmeRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public IList<Filme> TodosFilmes()
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "select * from filme";

                var reader = cmd.ExecuteReader();
                var filmes = new List<Filme>();

                while (reader.Read())
                {
                    var oNomeFilme = reader.GetOrdinal("nome_filme");
                    var oDuracao = reader.GetOrdinal("duracao");
                    var oGenero = reader.GetOrdinal("genero");
                    var oPrecoDia = reader.GetOrdinal("preco_dia");
                    var oResumo = reader.GetOrdinal("resumo");
                    var oId = reader.GetOrdinal("id");

                    var Filme = new Filme(reader.GetString(oNomeFilme), reader.GetString(oDuracao), reader.GetString(oGenero), reader.GetDecimal(oPrecoDia), reader.GetString(oResumo))
                    {
                        Id = reader.GetInt32(oId)
                    };

                    filmes.Add(Filme);
                }

                return filmes;
            }
        }

        public void SalvarFilme(Filme filme)
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var insert = "insert into filmes(nome_filme,duracao,genero,preco_dia,resumo) values(:nome_filme, :duracao, :genero, :preco_dia, :resumo)";

                var cmdInsert = new SQLiteCommand(insert, conexao);
                cmdInsert.Parameters.Add(":nome_filme", DbType.String).Value = filme.Nome;
                cmdInsert.Parameters.Add(":duracao", DbType.String).Value = filme.Duracao;
                cmdInsert.Parameters.Add(":genero", DbType.String).Value = filme.Genero;
                cmdInsert.Parameters.Add(":preco_dia", DbType.Decimal).Value = filme.PrecoDia;
                cmdInsert.Parameters.Add(":resumo", DbType.String).Value = filme.Resumo;

                cmdInsert.ExecuteNonQuery();

                var select = "select max(id) from filmes";
                var cmdSelect = new SQLiteCommand(select, conexao);

                filme.Id = int.Parse(cmdSelect.ExecuteScalar().ToString());
            }
        }
    }
}
