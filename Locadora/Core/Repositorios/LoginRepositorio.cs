﻿using Locadora.Core.Db;
using Locadora.Core.Models;

namespace Locadora.Core.Repositorios
{
    public class LoginRepositorio
    {
        private ConexaoFactory _conexaoFactory;

        public LoginRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public Usuario ObterUsuarioPeloLogin(string login)
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var cmd = conexao.CreateCommand();
                cmd.CommandText = "select id,login,senha from usuario where login = :login";

                cmd.Parameters.AddWithValue(":login", login);
                var reader = cmd.ExecuteReader();

                if (!reader.Read())
                {
                    return null;
                }

                var usuario = new Usuario();

                usuario.Id = reader.GetInt32(0);
                usuario.Login = reader.GetString(1);
                usuario.Senha = reader.GetString(2);

                return usuario;
            }
        }
    }
}