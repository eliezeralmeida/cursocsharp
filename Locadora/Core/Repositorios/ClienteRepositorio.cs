﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Threading.Tasks;
using Locadora.Core.Db;
using System.Data;
using Locadora.Core.Models;

namespace Locadora.Core.Repositorios
{
    public class ClienteRepositorio
    {
        private readonly ConexaoFactory _conexaoFactory;

        public ClienteRepositorio()
        {
            _conexaoFactory = ConexaoFactory.Instancia;
        }

        public void SalvarCliente(Cliente cliente)
        {
            using (var conexao = _conexaoFactory.Abrir())
            {
                var insert = "insert into cliente(nome,cpf,rg) values(:nome, :cpf, :rg)";

                var cmdInsert = new SQLiteCommand(insert, conexao);

                cmdInsert.Parameters.Add(":nome", DbType.String).Value = cliente.Nome;
                cmdInsert.Parameters.Add(":cpf", DbType.String).Value = cliente.Cpf;
                cmdInsert.Parameters.Add(":rg", DbType.String).Value = cliente.Rg;

                cmdInsert.ExecuteNonQuery();
                
                var select = "select max(id) from cliente";
                var cmdSelect = new SQLiteCommand(select, conexao);
                var ultimoId = cmdSelect.ExecuteScalar().ToString();

                cliente.Id = int.Parse(ultimoId);
            }
        }
    }
}
