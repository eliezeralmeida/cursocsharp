﻿namespace AprendendoOO
{
    public interface IAlfabetizado
    {
        string Nome();
        string EscreverAlgo();
    }
}