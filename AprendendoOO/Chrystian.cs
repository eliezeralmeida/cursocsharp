using System;

namespace AprendendoOO
{
    public class Chrystian : IAlfabetizado
    {
        public string Lapis()
        {
            return "Chrystian gosta de l�pis";
        }

        public string Nome()
        {
            return "Chrystian";
        }

        public string EscreverAlgo()
        {
            return Lapis();
        }
    }
}