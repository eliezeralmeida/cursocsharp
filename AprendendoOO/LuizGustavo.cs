﻿namespace AprendendoOO
{
    public class LuizGustavo : IAlfabetizado
    {
        public string Caneta()
        {
            return "Luiz escreveu isso no papel";
        }

        public string Nome()
        {
            return "Luiz Gustavo";
        }

        public string EscreverAlgo()
        {
            return Caneta();
        }
    }
}