namespace AprendendoOO
{
    public class Cassio : IAlfabetizado
    {
        public string Canetao()
        {
            return "Cassio escrevi isso";
        }

        public string Nome()
        {
            return "Cassio";
        }

        public string EscreverAlgo()
        {
            return Canetao();
        }
    }
}