﻿namespace AprendendoOO
{
    public class Cachorro : IAlfabetizado
    {
        public string Nome()
        {
            return "Rex";
        }

        public string EscreverAlgo()
        {
            return "Sei escrever, sou um cachorro loco!";
        }
    }
}