﻿using System;

namespace AprendendoOO
{
    public class EscrevaPraMin
    {
        public void EscreverNaTela(string escrevaIsso)
        {
            //ESSE METODO Ë UMA ACAO;
            Console.WriteLine("Escrevi na tela isso: " + escrevaIsso);
        }

        public void EscreverNaTelaComUmAlfatetizado(IAlfabetizado alfabetizado)
        {
            var nomeAlfabetizado = alfabetizado.Nome();
            var escreveu = alfabetizado.EscreverAlgo();

            Console.WriteLine(nomeAlfabetizado + " escreve: " + escreveu);
        }
    }
}