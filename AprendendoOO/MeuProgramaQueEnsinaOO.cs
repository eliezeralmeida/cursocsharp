﻿using System;
using System.Diagnostics.Tracing;

namespace AprendendoOO
{
    public class MeuProgramaQueEnsinaOO
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escrevi na tela isso: Olá mundo 1");
            Console.WriteLine("Escrevi na tela isso: Olá mundo 2");

            var escrevedor = new EscrevaPraMin();

            escrevedor.EscreverNaTela("Olá mundo 3");
            escrevedor.EscreverNaTela("Olá mundo 4");
            escrevedor.EscreverNaTela("Olá mundo 5");

            //exemplo orientado objeto mais ou menos
            var luiz = new LuizGustavo();
            var textoLuiz = luiz.Caneta();

            escrevedor.EscreverNaTela(textoLuiz);

            var chyrstian = new Chrystian();
            var textoChrystian = chyrstian.Lapis();
            escrevedor.EscreverNaTela(textoChrystian);

            //exemplo orientado objeto mais ou menos 2
            Console.ReadLine();

            escrevedor.EscreverNaTelaComUmAlfatetizado(new LuizGustavo());
            escrevedor.EscreverNaTelaComUmAlfatetizado(new Chrystian());
            escrevedor.EscreverNaTelaComUmAlfatetizado(new Cassio());
            escrevedor.EscreverNaTelaComUmAlfatetizado(new Cachorro());

            Console.ReadKey();
        }
    }
}